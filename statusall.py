#!/usr/bin/env python3

import os
import git
import sys
from termcolor import colored

def check_statuses(path, verbose=False):
    git_folder_found = False
    for root, dirs, files in os.walk(path):
        if git.repo.fun.is_git_dir(root+'/.git'):
            git_folder_found = True
            # print('\n\n\n**************\n{}\n**************\n'.format(root))
            output = git.cmd.Git(root).status().split('\n')
            branch = output[0].split(' ')[-1] # On branch master
            status = output[1]
            color = 'yellow'
            if output[-1][:37] == 'nothing to commit, working tree clean': #\
                    # and output[1][:20] != 'Your branch is ahead':
                color = 'green'
                if output[1][:20] == 'Your branch is ahead':
                    color = 'yellow'
            else:
                # for i in range(2,min([10, len(output)])):
                for i in range(2, len(output)):
                    status += '\nx\t' + output[i]
                # status += '\n\n\t ... \n\n'
                color = 'red'
            # print(colored("{:<48}\n --> {:<20}: {}".format(root, branch, status), color))
            print(colored(root, color))
            if verbose:
                print(" --> {:<20}: {}".format(branch, status))
    if not git_folder_found:
        print('no git folder found at path {}'.format(path))
        
if __name__ == "__main__":

    current = os.getcwd()
    path_list = [current]
    args = sys.argv[1:]

    verbose = False
    if len(args)>0 and args[0]=='-v':
        verbose = True
        args = args[1:]

    if len(args)>0:
        path_list = []
        for a in args:
            if a[0]=='/':
                path_list.append(a)
            else:
                path_list.append(current + '/' + a)
    for path in path_list:
        check_statuses(path, verbose)
